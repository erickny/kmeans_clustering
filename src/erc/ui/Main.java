package erc.ui;

import erc.core.KMeans;
import erc.core.Point;
import erc.libs.Helpers;
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.ScatterChart;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class Main extends Application {

    @Override
    public void start(Stage primaryStage) {
        Parent root = plotData(primaryStage);
        Scene scene = new Scene(root, 750, 500);
        scene.getStylesheets().add(this.getClass().getResource("custom.css").toExternalForm());

        primaryStage.setScene(scene);
        primaryStage.setTitle("Clustering - App");
        primaryStage.show();
    }

    private BorderPane plotData(Stage stage) {

        BorderPane root = new BorderPane();
        root.setPadding(new Insets(20));

        ///
        String path = "datasets/agrico.csv";
        ArrayList<Point> data = Helpers.loadData(path);

        ArrayList<Point> centroids = new ArrayList<>();
        centroids.add(new Point("C1", 0.94, 1.02));
        centroids.add(new Point("C2", 1.15, 1.13));
        centroids.add(new Point("C3", 2.12, 1.52));

        /// compute clustering
        KMeans kMeans = new KMeans(3, data, centroids);
        ArrayList<ArrayList<Point>> clusters = kMeans.run();
        centroids = kMeans.getCentroids();

        ///
        NumberAxis xAxis = new NumberAxis();
        NumberAxis yAxis = new NumberAxis();

        ScatterChart<Number, Number> scatterChart = new ScatterChart<>(xAxis, yAxis);
        scatterChart.setTitle("Clustering");

        XYChart.Series<Number, Number> centroidsData = new XYChart.Series<>();
        centroidsData.setName("Centroids");

        XYChart.Series<Number, Number> clusterData;

        int i = 0;
        for (final ArrayList<Point> cluster: clusters) {
            clusterData = new XYChart.Series<>();
            clusterData.setName("Cluster " + (++i));

            for (final Point point: cluster)
                clusterData.getData().add(new XYChart.Data<>(point.getX(), point.getY()));

            scatterChart.getData().add(clusterData);
        }

        for (final Point c: centroids)
            centroidsData.getData().add(new XYChart.Data<>(c.getX(), c.getY()));

        scatterChart.getData().add(centroidsData);

        for (final XYChart.Series<Number, Number> series: scatterChart.getData()) {
            for (final XYChart.Data<Number, Number> dt: series.getData()) {
                Tooltip tooltip = new Tooltip();
                tooltip.setText(new DecimalFormat("0.00").format(dt.getXValue()) + ", " +
                        new DecimalFormat("0.00").format(dt.getYValue()));
                Tooltip.install(dt.getNode(), tooltip);
            }
        }

        root.setCenter(scatterChart);
        return root;
    }

    public static void main(String[] args) {
        Application.launch(args);
    }
}