package erc.core;

/**
 * Ny Aina Erick on Thu Nov 1, 2018 13:15 PM
 * */
public class Point {

    private double x;
    private double y;
    private String label;

    public Point() {
        this(0,0);
    }

    public Point(double x, double y) {
        this("", x, y);
    }

    public Point(String label, double x, double y) {
        this.label = label;
        this.x = x;
        this.y = y;
    }

    public String getLabel() {
        return this.label;
    }

    public double getX() {
        return this.x;
    }

    public double getY() {
        return this.y;
    }

    public void setX(double x) {
        this.x = x;
    }

    public void setY(double y) {
        this.y = y;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    @Override
    public String toString() {
        String result = String.format("%s\t%.2f\t%.2f", this.getLabel(), this.getX(), this.getY());
        return result;
    }

}
